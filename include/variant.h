#pragma once
#include "Type.h"
namespace lg
{
	
	class variant
	{
	public:
	    // these are immutable and 
	    // are assumed not to own pointers
	    // passed to them
	    template <typename T>
	    variant(T val)
	    {
	        As<T>() = val;
	        storedType = TYPE(T);
	    }

        variant(const variant& rhs)
        {
            storedType = rhs.Type();
            uvar = rhs.uvar;
        }

        variant& operator=(const variant& rhs)
        {
            storedType = rhs.Type();
            uvar = rhs.uvar;
        }

	    lg::Type Type() const
	    {
	        return storedType;
	    }
	
	    template <typename T>
	    bool SafeCast(T& output)
	    {
	        if(TYPE(T) == storedType)
	        {
	            output = As<T>();
	            return true;
	        }
	        else
	        {
	            return false;
	        }
	    }
	
	
	    // TODO: T as reference?  Not sure if possible
	    template <typename T>
	    T& As()
	    {
	        return *(T*)(&uvar.iVal);
	    }

	private:
	    // type of object that we're storing
	    lg::Type storedType;
	    union
	    {
	        // TODO: what do we really care about here?
	        char cVal;
            float fVal;
	        int iVal;
	        bool bVal;
	        void* ptr;
	    } uvar;
	};
};