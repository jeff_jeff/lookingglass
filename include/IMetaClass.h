#pragma once
#include "Type.h"
#include <tchar.h>
#include <string>
#include <unordered_map>

// declaring meta classes

// need virtual meta class
namespace lg
{
    class MetaRegistry;
    class IMetaProperty;

    class IMetaClass
    {
    public:
        IMetaClass(Type t, TCHAR* cn, size_t s, 
            Type bc = GetTypeID<NullType>());

        Type ClassType() const;
        const TCHAR* Name() const;
        size_t Size() const;
        bool IsA(Type t);
        virtual void Initialize(MetaRegistry& reg) = 0;
        IMetaProperty* AddProperty(IMetaProperty* prop);
    private:
        Type type;
        const TCHAR* className;
        size_t classSize;
        // TODO: multiple inheritance
        // TODO: better to store MetaClass* here to simplify lookup
        // but not possible due to static initialization fiasco, maybe cache on lookup instead?
        Type baseClass;

        // data members
        // properties (get/set)
        std::unordered_map<std::string, IMetaProperty*> propertyMap;
        // methods
    };


    template <typename T>
    class MetaClass : public IMetaClass
    {
    public:
        MetaClass(Type t, TCHAR* cn, size_t s, 
            Type bc = GetTypeID<NullType>())
            : IMetaClass(t, cn, s, bc)
        {

        }

        typedef T SelfType;
        
        virtual void Initialize(MetaRegistry& reg);
    };


    template <typename T>
    class MetaInfoStorage
    {
        // Thanks to Sean Middleditch (http://seanmiddleditch.com/journal/) for
        // inspiring the design of this class
    public:
        static IMetaClass& GetMeta()
        {
            return *metaClass;
        }
    private:
        friend class MetaRegistry;
        static IMetaClass* metaClass;
    };

};

// this is the simplest way to declare that meta exists for a class.  You can declare this
// on classes that you don't own, either


// and another hack.  Why is this necessary?  Why?
#define DM2(count) DECL_META_##count
#define DM1(count) DM2(count)
#define DM(count) DM1(count)

// now combine all the hacks with the other hacks
#define DeclareMeta(...) \
    GLUE(DM(VA_NUM_ARGS(__VA_ARGS__)), (__VA_ARGS__))

// These are the actual meta declarations
#define DECL_META_2(X, Y) lg::IMetaClass* lg::MetaInfoStorage<X>::metaClass = \
    lg::MetaRegistry::GetInstance().CreateAndRegisterMeta<X>(L#X, TYPE(Y));\
    void lg::MetaClass<X>::Initialize(lg::MetaRegistry& reg)

#define DECL_META_1(X) lg::IMetaClass* lg::MetaInfoStorage<X>::metaClass = \
    lg::MetaRegistry::GetInstance().CreateAndRegisterMeta<X>(L#X);\
    void lg::MetaClass<X>::Initialize(lg::MetaRegistry& reg)

// give meta to particular class as virtual function but still uses
// meta factory lookup.  This allows inheritance
#define VIRTUAL_GETMETA(X) virtual lg::IMetaClass& GetMeta() { return lg::MetaInfoStorage<X>::GetMeta(); }

// Meta lookup

// detect if a GetMeta() method exists
template <typename T>
struct ClassHasVirtualGetMeta
{
    // match what we want
    template <typename U>
    static lg::NullType HasVirtualGetMeta( decltype( ( (U*)(NULL) )->GetMeta() )*);

    // match whatever else
    template <typename U>
    static int HasVirtualGetMeta(...);

    // let compiler figure out best match
    static const bool value = std::is_same<lg::NullType, decltype(HasVirtualGetMeta<T>(NULL))>::value;
};

template <typename T>
struct MetaLookup
{
    template <typename U>
    static typename std::enable_if<ClassHasVirtualGetMeta<U>::value, lg::IMetaClass&>::type GetMetaImpl(U& val)
    {
        // we do have getmeta 
        return val.GetMeta();

    }

    template <typename U>
    static typename std::enable_if<!ClassHasVirtualGetMeta<U>::value, lg::IMetaClass&>::type GetMetaImpl(U& val)
    {
        return lg::MetaInfoStorage<U>::GetMeta();
    }

    static lg::IMetaClass& resolve(T& val)
    {
        return GetMetaImpl<T>(val);
    }

    static lg::IMetaClass& resolve(T* val)
    {
        return GetMetaImpl<T>(*val);
    }
};

// for getting meta on a variable
#define OBJECT_META(X) MetaLookup<std::remove_cv<decltype(X)>::type>::resolve(X)


template <typename T>
struct HasStaticMeta
{

    template <typename U>
    static lg::NullType CheckStaticMeta( decltype(T::GetTypeMeta())*);

    template <typename U>
    static int CheckStaticMeta(...);

    static const bool value = std::is_same<lg::NullType, decltype(CheckStaticMeta<T>(NULL))>::value;

};


template <typename T>
struct ResolveTypeMeta
{
    template <typename U>
    static typename std::enable_if<HasStaticMeta<U>::value, lg::IMetaClass&>::type GetMeta()
    {
        return U::GetTypeMeta();
    };

    template <typename U>
    static typename std::enable_if<!HasStaticMeta<U>::value, lg::IMetaClass&>::type GetMeta()
    {
        return lg::MetaInfoStorage<U>::GetMeta();
    };

    static lg::IMetaClass& resolve()
    {
        return GetMeta<T>();
    };    
};

// Get meta for some type.  Will NOT work on instances of some object
#define TYPE_META(X) ResolveTypeMeta<X>::resolve()