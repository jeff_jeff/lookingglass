#pragma once
#include "variant.h"
#include <string>
#include <sstream>
// this is for property getter/setters
// getter and setter are both optional

namespace lg
{
    class IMetaProperty
    {
    public:
        IMetaProperty(const TCHAR* name, Type type)
            : propertyName(name), propertyType(type)
        {

        }
        const TCHAR* PropertyName() const
        {
            return propertyName;
        }

        const Type PropertyType() const
        {
            return propertyType;
        }

        virtual variant Get(void* classInstance) = 0;
        virtual void Set(void* classInstance, variant& val) = 0;
        virtual bool ReadOnly() const = 0;
        virtual std::wstring ToString(void* classInstance) = 0;
        

    protected:

    private:
        const TCHAR* propertyName;
        const Type propertyType;
    };


    // TODO: pointers/reference types
    // TODO: const correctness (or not) on properties
    // concrete implementations
    template <typename CLASSTYPE, typename PROPTYPE,
                typename GETTER, typename SETTER = NullType*>
    class MetaProperty : public IMetaProperty
    {
    public:
        //typedef PROPTYPE (CLASSTYPE::*GETTER)();
        //typedef void (CLASSTYPE::*SETTER)(PROPTYPE);

        MetaProperty(const TCHAR* propName, 
            GETTER g, 
            SETTER s = NULL)
            : IMetaProperty(propName, TYPE(PROPTYPE)), 
            getter(g), 
            setter(s)
        {
            
        }

        static const bool IsClass = std::is_class<PROPTYPE>::value;

        virtual bool ReadOnly() const
        {
            return setter == NULL;
        }

        virtual variant Get(void* classInstance)
        {
            return variant(GetValue(*reinterpret_cast<CLASSTYPE*>(classInstance)));
        }

        virtual void Set(void* classInstance, variant& val)
        {
            SetValue(*reinterpret_cast<CLASSTYPE*>(classInstance),
                val.As<PROPTYPE>());
        }

        PROPTYPE GetValue(CLASSTYPE& cls)
        {
            return (cls.*getter)();
        }

        void SetValue(CLASSTYPE& t, PROPTYPE val)
        {
           InternalSet<SETTER>(t, val);
        }

        // TODO: what to do for class types?
        std::wstring ToString(void* classInstance)
        {
            std::wstringstream str;
            str << GetValue(*(CLASSTYPE*)classInstance );
            return str.str();
        }

    private:

        // ToString:  possibilities
        // is class (by val)
        // is pointer to class (is null)
        // is pointer to data
        

        // if setter
        template <typename U>
        typename std::enable_if<!std::is_same<U, NullType*>::value, void>::type  
        InternalSet(CLASSTYPE& cls, PROPTYPE val)
        {
            if(setter)
                (cls.*setter)(val);
        }

        // read-only
        template <typename U>
        typename std::enable_if<std::is_same<U, NullType*>::value, void>::type
        InternalSet(CLASSTYPE& cls, PROPTYPE val)
        {

        }

        GETTER getter;
        SETTER setter;
    };
};



// determines the return type of a property on a class.  We use the 
// getter since we're guaranteed to have one (write-only is undefined)
#define PROP_TYPE(CLASS, NAME) decltype( ((CLASS*)NULL)->NAME() )

// This is the variable type for a property with a getter/setter
#define DECLPROP(CLASS, NAME, GET, SET) lg::MetaProperty<CLASS, PROP_TYPE(CLASS, GET),\
    decltype(&CLASS::GET), decltype(&CLASS::SET)>

// variable type for a property that is read-only
#define DECLPROP_READONLY(CLASS, NAME, GET) lg::MetaProperty<CLASS, PROP_TYPE(CLASS, GET), decltype(&CLASS::GET)>

// variable type for a property that is overloaded
#define DECLPROP_OVERLOAD(CLASS, NAME) lg::MetaProperty<CLASS, PROP_TYPE(CLASS, NAME),\
    decltype(static_cast<PROP_TYPE(CLASS, NAME) (CLASS::*)()>(&CLASS::NAME)),\
    decltype(static_cast<void (CLASS::*)(PROP_TYPE(CLASS, NAME))>(&CLASS::NAME))>

// actual definition of the property; you could set a value to this or initialize
// a pointer to this
#define CREATE_PROP(CLASS, NAME, GET, SET) lg::MetaProperty<CLASS, PROP_TYPE(CLASS, GET),\
    decltype(&CLASS::GET), decltype(&CLASS::SET)>(L#NAME, &CLASS::GET, &CLASS::SET)

// same as CREATE_PROP but for properties that have an overloaded getter/setter
#define CREATE_PROP_OVERLOAD(CLASS, NAME) lg::MetaProperty<CLASS, PROP_TYPE(CLASS, NAME),\
    decltype(static_cast<PROP_TYPE(CLASS, NAME) (CLASS::*)()>(&CLASS::NAME)),\
    decltype(static_cast<void (CLASS::*)(PROP_TYPE(CLASS, NAME))>(&CLASS::NAME))>\
    (L#NAME,\
    &CLASS::NAME,\
    &CLASS::NAME)

// same as CREATE_PROP but with only a getter
#define CREATE_PROP_READ_ONLY(CLASS, NAME, GET) lg::MetaProperty<CLASS, PROP_TYPE(CLASS, GET), \
    decltype(&CLASS::GET)>\
    (L#NAME, &CLASS::GET, NULL)

// same as PROPERTY but for properties that only have a getter
#define PROPERTY_READ_ONLY(CLASS, NAME, GET) auto CLASS##_##GET##_##READONLY_##NAME =\
    new CREATE_PROP_READ_ONLY(CLASS, NAME, GET)

// this actually creates a property with a variable name
#define PROPERTY(CLASS, NAME, GET, SET) auto CLASS##_##GET##_##SET_##NAME =\
    new CREATE_PROP(CLASS, NAME, GET, SET)

// same as PROPERTY but for properties with overloaded getter/setter
#define PROPERTY_OVERLOAD(CLASS, NAME) auto CLASS##_##NAME = \
    CREATE_PROP_OVERLOAD(CLASS, NAME)



// more hacks for __VA_ARGS__ issues in MSVC
#define DPRP2(count) PROP_##count
#define DPRP1(count) DPRP2(count)
#define DPRP(count) DPRP1(count)


// Property(GET) - get only or get/set is same name
// Property(GET,SET) - get/set with different names (works with same name also though)
#define Property(...) GLUE(DPRP(VA_NUM_ARGS(__VA_ARGS__)), (__VA_ARGS__))

// Used in context of DeclareMeta(class) so we can
// count on certain things being available to these macros as follows:
// MetaRegistry& reg
// typedef CLASS SelfType
#define PROP_1(GET) auto* prop_##GET = \
    new lg::MetaProperty<SelfType, PROP_TYPE(SelfType, GET),\
    decltype(static_cast<PROP_TYPE(SelfType, GET) (SelfType::*)()>(&SelfType::GET)),\
    decltype(static_cast<void (SelfType ::*)(PROP_TYPE(SelfType , GET))>(&SelfType ::GET))>\
    (L#GET,\
    &SelfType ::GET,\
    &SelfType ::GET);  this->AddProperty(prop_##GET)

#define PROP_2(GET, SET)  lg::MetaProperty<SelfType, \
decltype(  ((SelfType*)NULL)->GET()) , \
    decltype (&SelfType::GET), \
    decltype(&SelfType::SET)>* prop_##GET = \
    new lg::MetaProperty<SelfType, \
    decltype(  ((SelfType*)NULL)->GET()), \
    decltype (&SelfType::GET), \
    decltype(&SelfType::SET)>(\
    L#GET, \
    &SelfType::GET, \
    &SelfType::SET); this->AddProperty(prop_##GET)

