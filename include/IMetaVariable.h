#pragma once

// this is for class data members.  Will work for public and
// private so long as the private member declarations are on an 
// InitMeta method of the class itself

namespace lg
{

    class IMetaVariable
    {
    public:

    private:
    };
};
