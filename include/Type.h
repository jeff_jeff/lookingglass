#pragma once
#include <type_traits>


// HACK:  Because Microsoft C++ compiler team can't seem to fix 
// bug  380090 or bug 521844 we have no choice but to do this awful thing.
// tldr- Microsoft passes __VA_ARGS__ as a single argument instead of expanding

// need this to force the preprocessor to re-evaluate __VA_ARGS__ (at least I think that's what it's doing)
#define GLUE(A, B) A B

// standard arg counting
#define VA_NUM_ARGS(...) VA_NUM_ARGS_IMPL_((__VA_ARGS__, 2,1))
// and the hack again
#define VA_NUM_ARGS_IMPL_(tuple) VA_NUM_ARGS_IMPL tuple
#define VA_NUM_ARGS_IMPL(_1,_2,N,...) N

namespace lg
{
    // TODO: #ifdef NOSTL
    // do we want that?
   
    // Null Object pattern: use TYPE(NullType) 
    // if you need some sort of default value
    struct NullType
    {

    };

    // defines a type
    class Type
    {
    public:
        Type();
        Type(const Type& rhs);
        Type& operator=(const Type& rhs);
        bool operator==(const Type& rhs);
        bool operator==(const unsigned rhs);
        bool operator!=(const Type& rhs);
        bool operator!=(const unsigned rhs);
        
        operator unsigned () const;
    private:
        unsigned id;
    };
    
    template <typename T>
    struct TypeHasher
    {
        size_t operator()(const lg::Type& t) const
        {
            return std::hash<unsigned>()((unsigned)t);
        }
    };

    // Generates a monotonically increasing 
    // unsigned integer value
    class MonotonicUnsigned
    {
    public:
        // thread safety: each call to this
        // function will return a new unsigned
        // integer value, even if called on 
        // multiple threads.  No guarantee is
        // made about the ordering or continuity 
        // of the values returned on multiple calls
        static unsigned GetNext();
  
    };

    // this will generate a unique Type for every 
    // possible type that you ever want to identify, even
    // if no meta has been declared on it.  It's important
    // to use the macros defined below so that pointers, const, 
    // volatile, etc. all get the same Type when we try to 
    // identify them
    template <typename T>
    Type GetTypeID()
    {
         static Type tid;
         return tid;
    }

    // useful macros are useful:

    // Get the Type for a particular type e.g. TYPE(int)
    #define TYPE(x) lg::GetTypeID<x>()

    // Get the Type of an arbitrary variable e.g. int x; TYPEOF(x),
    // useful for lookup of type of a particular variable to get meta, since 
    // metaclass for "int" is always going to be the same and we don't want 
    // different meta for const int or int* and so on
    #define TYPEOF(x) TYPE(std::remove_cv<std::remove_pointer<decltype(x)>::type>::type)

    // together, you might do something like this:
    // int x;
    // if(TYPEOF(x) == TYPE(int))
};