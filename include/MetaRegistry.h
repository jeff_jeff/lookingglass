#pragma once
#include <unordered_map>
#include "Type.h"
#include <tchar.h>
namespace lg
{

    class IMetaClass;

    // place to hold meta classes
    class MetaRegistry
    {
    public:
        // dislike singleton but this allows a much more elegant
        // solution for creating and initializing metaclass stuff
        static MetaRegistry& GetInstance();        
        ~MetaRegistry();
        IMetaClass* GetMeta(Type type) const;
        void RegisterMeta(const IMetaClass* mc);

        template<typename T>
        IMetaClass* CreateAndRegisterMeta(TCHAR* name, Type btype = GetTypeID<NullType>())
        {
            // TODO: allocating memory for these things ought to be done efficiently.  Not sure 
            // what that's going to be just yet though
            Type classType = TYPE(T);
            MetaClass<T>* metacls = new MetaClass<T>(classType, name, sizeof(T), btype);
            this->metaMap[classType] = metacls;
            return metacls;
        }
        
        // initializes everything declared
        void Initialize();
    private:
        MetaRegistry();
        // do not allow these
        MetaRegistry(const MetaRegistry&);
        MetaRegistry& operator=(const MetaRegistry&);
        std::unordered_map<Type, IMetaClass*, lg::TypeHasher<lg::Type>> metaMap;
    };
};