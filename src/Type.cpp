#include "../include/Type.h"
#include <atomic>

namespace lg
{


    Type::Type()
        : id(MonotonicUnsigned::GetNext())
    {

    }

    Type::Type(const Type& rhs) 
        : id(rhs.id)
    {

    }

    Type& Type::operator=(const Type& rhs)
    {
        if(this != &rhs)
            id = rhs.id;
        
        return *this;
    }

    Type::operator unsigned() const
    {
        return id;
    }

    bool Type::operator==( const Type& rhs )
    {
        return rhs.id == id;
    }

    bool Type::operator==( const unsigned rhs )
    {
        return id == rhs;
    }

    bool Type::operator!=( const unsigned rhs )
    {
        return rhs != id;
    }

    bool Type::operator!=(const Type& rhs)
    {
        return rhs.id != id;
    }

    unsigned MonotonicUnsigned::GetNext()
    {
        static std::atomic<unsigned> curVal = 0;
        unsigned val = ++curVal;
        return val;
    }
}