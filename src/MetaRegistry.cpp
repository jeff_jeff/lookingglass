#include "../include/MetaRegistry.h"
#include "../include/IMetaClass.h"
#include "../include/Type.h"

namespace lg
{

    MetaRegistry& MetaRegistry::GetInstance()
    {
        static MetaRegistry instance;
        return instance;
    }

    MetaRegistry::MetaRegistry()
    {

    }

    // TODO: need to make sure that this is destroyed somehow
    MetaRegistry::~MetaRegistry()
    {
        for(auto iter : metaMap)
        {
            delete iter.second;
        }
    }

    IMetaClass* MetaRegistry::GetMeta( Type type ) const
    {
        return NULL;
    }

    void MetaRegistry::RegisterMeta( const IMetaClass* mc )
    {

    }


    void MetaRegistry::Initialize()
    {
        for(auto iter : metaMap)
        {
            iter.second->Initialize(*this);
        }
    }

};
