#include "../include/IMetaClass.h"

namespace lg
{
	
	lg::IMetaClass::IMetaClass( Type t, TCHAR* cn, size_t s, Type bt) : 
	    type(t), className(cn), classSize(s), baseClass(bt)
	{
	
	}
	
	lg::Type lg::IMetaClass::ClassType() const
	{
	    return type;
	}
	
	const TCHAR* lg::IMetaClass::Name() const
	{
	    return className;
	}
	
	size_t lg::IMetaClass::Size() const
	{
	    return classSize;
	}
	
	bool lg::IMetaClass::IsA( Type t )
	{
	    if(t == type)
	    {
	        return true;
	    }
	    else if(baseClass != TYPE(NullType))
	    {
	        return baseClass == t;
	    }
	    else
	    {
	        return false;
	    }
	}
	
	IMetaProperty* lg::IMetaClass::AddProperty( IMetaProperty* prop )
	{
	    return NULL;
	}
	
}
