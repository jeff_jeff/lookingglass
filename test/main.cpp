// LookingGlass.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int _tmain(int argc, _TCHAR* argv[])
{    
    ::testing::InitGoogleMock(&argc, argv);  
    lg::MetaRegistry::GetInstance().Initialize();

    auto qq = RUN_ALL_TESTS();
    std::cin.get();
    return qq;
}

class IntegrationTestClass
{
public:
    int IntProp()
    {
        return intProp;
    }

    void IntProp(int val)
    {
        intProp = val;
    }

    int IProp(){ return intProp; }

    void IPropSet(int v)
    {
        intProp = v;
    }

    int IPropReadOnly()
    {
        return intProp;
    }

    int otherIntProp;
private:
    int intProp;
};

DeclareMeta(IntegrationTestClass)
{
    // simplest case:  property with getter/setter different names
    Property(IProp, IPropSet);

    // simple case: property with getter/setter same name.
    Property(IntProp);

    // much harder: property with only Get
    // not sure if possible with one macro
    // Property(IPropReadOnly);

}

TEST(Integration, IntegrationTest)
{

}

