#include "stdafx.h"
#include "MetaTestDoubles.h"


class Inheritance : public ::testing::Test
{
protected:
    void SetUp()
    {

    }

    void TearDown()
    {

    }

};

TEST_F(Inheritance, IsA_SameType_IsSame)
{
    lg::IMetaClass& mc = TYPE_META(someTestClass);
    EXPECT_TRUE(mc.IsA(TYPE(someTestClass)));

}

TEST_F(Inheritance, IsA_DerivedClass_IsDerivedFromBase)
{
    lg::IMetaClass& mc = TYPE_META(someDerivedClass);
    EXPECT_TRUE(mc.IsA(TYPE(someOtherTestClass)));
}