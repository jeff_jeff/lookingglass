#include "stdafx.h"
#include "MetaTestDoubles.h"


// NOTE: these declare static instances of IMetaClass or define some implementation 
// of a method, so they MUST always go in a source file and shouldn't be declared in 
// headers ever.  

// metadata for someTestClass can be declared anywhere that someTestClass
// is in scope.
DeclareMeta(someTestClass)
{
    std::cout << "initialized sometestclass" << std::endl;
}

DeclareMeta(someOtherTestClass)
{
    std::cout << "initialized someOtherTestClass" << std::endl;
}

DeclareMeta(someDerivedClass, someOtherTestClass)
{

}

DeclareMeta(baseClassVirtual)
{

}

DeclareMeta(derivedClassVirtual)
{

}