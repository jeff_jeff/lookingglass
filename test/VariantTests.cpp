#include "stdafx.h"
#include "../include/variant.h"


TEST(VariantTest, VariantCanStorePrimitives)
{
    unsigned val = 10;
    lg::variant v(val);

    unsigned outval = v.As<unsigned>();
    EXPECT_EQ(val, outval);
    EXPECT_EQ(TYPE(unsigned), v.Type());

}

TEST(VariantTest, SafeCastIsSafe)
{
    unsigned val = 10;
    lg::variant SUT(val);
    unsigned output;
    bool cast_ok = SUT.SafeCast<unsigned>(output);
    ASSERT_TRUE(cast_ok);
    EXPECT_EQ(val, output);
}

/*
// TODO: get this to work somehow
TEST(VariantTest, VariantCanStoreReference)
{
    int value = 33;
    int& valRef = value;
    lg::variant SUT(valRef);

    int& outRef = SUT.As<int&>();
    EXPECT_EQ(value, outRef);
    valRef = 55;
    EXPECT_EQ(valRef, outRef);
}
*/