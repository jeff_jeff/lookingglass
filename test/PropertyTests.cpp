#include "stdafx.h"

class PropertyTestInternalClass
{

};

class PropertyTestClass
{
public:
    
    int IntProperty()
    {
        return intPropVal;
    }
    
    void IntProperty(int val)
    {
        intPropVal = val;
    }

    float FloatPropReadOnly() const
    {
        return floatPropVal;
    }

    int intPropVal;
    float floatPropVal;

    float* FloatPropPtr()
    {
        return &floatPropVal;
    }

    void FloatPropPtr(float* f)
    {
        floatPropVal = *f;
    }

    char GetChar() const
    {
        return charProp;
    }

    void SetChar(const char c)
    {
        charProp = c;
    }

    void SetInternalClassByVal(PropertyTestInternalClass c)
    {
        internalClassByVal = c;
    }

    PropertyTestInternalClass GetInternalClassByVal() 
    {
        return internalClassByVal;
    }

    void SetInternalClassPtr(PropertyTestInternalClass* c)
    {
        internalClassPtr = c;
    }

    PropertyTestInternalClass* GetInternalClassPtr() 
    {
        return internalClassPtr;
    }
    
    PropertyTestInternalClass internalClassByVal;
    PropertyTestInternalClass* internalClassPtr;
    PropertyTestInternalClass internalClassByRef;

    char charProp;
};

class MetaPropertyTests : public ::testing::Test
{
protected:
    void SetUp()
    {
       
        fakeTestClass.intPropVal = 42;
        fakeTestClass.floatPropVal = 1.f;
        
        testIntProp = new CREATE_PROP_OVERLOAD(PropertyTestClass, IntProperty);
        testFloatProp = 
            new CREATE_PROP_READ_ONLY(PropertyTestClass, FloatPropReadOnly, FloatPropReadOnly);

       // testClassByValProp = new CREATE_PROP(PropertyTestClass, InternalClassByVal, GetInternalClassByVal, SetInternalClassByVal);
       // testClassPtrProp = new CREATE_PROP(PropertyTestClass, InternalClassPtr, GetInternalClassPtr, SetInternalClassPtr);
       // testValPtrProp = new CREATE_PROP_OVERLOAD(PropertyTestClass, FloatPropPtr);

        /*   *************
        What this is actually doing without the macros:
        lg::MetaProperty<
            PropertyTestClass, 
            decltype( ((PropertyTestClass*)NULL)->FloatPropReadOnly() ),
            decltype( &PropertyTestClass::FloatPropReadOnly),
            decltype( &PropertyTestClass::UhOhNoSetter)>
        *testFloatProp =
        new lg::MetaProperty<
            PropertyTestClass, 
            decltype( ((PropertyTestClass*)NULL)->FloatPropReadOnly() ),
            decltype( &PropertyTestClass::FloatPropReadOnly),
            decltype( &PropertyTestClass::UhOhNoSetter)>
            (
            L"FloatPropReadOnly",
            &PropertyTestClass::FloatPropReadOnly, 
            NULL);

        
        
        
        */
    }

    void TearDown()
    {
        delete testIntProp;
        delete testFloatProp;
    }
    PropertyTestClass fakeTestClass;
    DECLPROP_OVERLOAD(PropertyTestClass, IntProperty)* testIntProp;
    DECLPROP_READONLY(PropertyTestClass, FloatPropReadOnly, FloatPropReadOnly)* 
        testFloatProp;
    DECLPROP(PropertyTestClass, InternalClassByVal, GetInternalClassByVal, SetInternalClassByVal)* testClassByValProp;
    DECLPROP(PropertyTestClass, InternalClassPtr, GetInternalClassPtr, SetInternalClassPtr)* testClassPtrProp;
    DECLPROP_OVERLOAD(PropertyTestClass, FloatPropPtr)* testValPtrProp;
};

TEST_F(MetaPropertyTests, MetaPropertyDeclaration_HasCorrectData)
{

}

TEST_F(MetaPropertyTests, PropertyGetAndSet_CanManipulateValue)
{
    int val = testIntProp->GetValue(fakeTestClass);
    EXPECT_EQ(42, val);

    testIntProp->SetValue(fakeTestClass, 55);
    EXPECT_EQ(55, fakeTestClass.intPropVal);
}

TEST_F(MetaPropertyTests, ReadOnlyProperty_CanGetValue_NoSet)
{
    float val = 47.f;
    testFloatProp->SetValue(fakeTestClass, val);
    EXPECT_FLOAT_EQ(1.f, fakeTestClass.FloatPropReadOnly());

    val = testFloatProp->GetValue(fakeTestClass);
    EXPECT_FLOAT_EQ(1.f, val);

}

TEST_F(MetaPropertyTests, BaseGetSet_WorkCorrectly)
{
    lg::variant val = testFloatProp->Get(&fakeTestClass);

    EXPECT_FLOAT_EQ(1.f, val.As<float>());

    int n = 22;
    lg::variant nval(n);
    testIntProp->Set(&fakeTestClass, nval);
    EXPECT_EQ(n, fakeTestClass.intPropVal);    
}

TEST_F(MetaPropertyTests, ToStringOnDataTypes_WorksCorrectly)
{
    fakeTestClass.floatPropVal = 37.2f;
    auto str = testFloatProp->ToString(&fakeTestClass);
    std::wstring val = L"37.2";
    EXPECT_EQ(val, str);

    fakeTestClass.IntProperty(42);
    str = testIntProp->ToString(&fakeTestClass);
    val = L"42";
    EXPECT_EQ(val, str);
}

TEST_F(MetaPropertyTests, PropertyGetSet_ClassByValue_WorksCorrectly)
{

}

TEST_F(MetaPropertyTests, PropertyGetSet_ClassByPtr_WorksCorrectly)
{

}

TEST_F(MetaPropertyTests, ToStringOnClassByValue_WorksCorrectly)
{

}

TEST_F(MetaPropertyTests, ToStringOnClassByPtr_WorksCorreectly)
{

}

TEST_F(MetaPropertyTests, ToStringOnDataTypePtr_WorksCorrectly)
{

}