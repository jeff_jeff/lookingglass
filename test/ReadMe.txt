========================================================================
    CONSOLE APPLICATION : LookingGlass Project Overview
========================================================================

This is the unit test project for LookingGlass.  It ensures that the functionality of the library is correct while also providing documentation as to how it can be used.  If you are using LookingGlass in your own projects, it is NOT necessary to include ANY code from the test project itself.
