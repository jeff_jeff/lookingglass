#include "stdafx.h"
//
//// TODO: only compile/run if NO_STL is defined
//TEST(TypeTest, CV_worksCorrectly)
//{
//
//    const int i = 0;
//    const volatile int j = 0;
//    volatile int k = 0;
//    const volatile int l[42] = {0};
//    
//
//    bool b = lg::are_same<int, lg::remove_cv<decltype(i)>::type>::value;
//    // not sure why I actually need b here, but gtest complains without it
//    EXPECT_TRUE(b) << "error on: lg::are_same<int, lg::remove_cv<const int>::type>::value";
//
//    b = lg::are_same<int, lg::remove_cv<decltype(j)>::type>::value;
//    EXPECT_TRUE(b) << "error on: lg::are_same<int, lg::remove_cv<const volatile int>::type>::value";
//
//    b = lg::are_same<int, lg::remove_cv<decltype(k)>::type>::value;
//    EXPECT_TRUE(b) << "error on: lg::are_same<int, lg::remove_cv<volatile int>::type>::value";
//
//    b = lg::are_same<int[42], lg::remove_cv<decltype(l)>::type>::value;
//    EXPECT_TRUE(b) << "error on: lg::are_same<int, lg::remove_cv<const volatile int[]>::type>::value";
//
//    b = lg::are_same<int[], lg::remove_cv<const volatile int[]>::type>::value;
//    EXPECT_TRUE(b) << "error on: lg::are_same<int[], lg::remove_cv<const volatile int[]>::type>::value";
//}
//
//
//TEST(TypeTest, NumericTypesWorkCorrectly)
//{
//    bool b = lg::is_floatingpoint<float>::value;
//    EXPECT_TRUE(b) << "Error: float";
//
//    b = lg::is_floatingpoint<double>::value;
//    EXPECT_TRUE(b) << "Error: double";
//
//    b = lg::is_floatingpoint<int>::value;
//    EXPECT_TRUE(!b) << "Error: int is not a floating point type";
//
//    b = lg::is_integral<float>::value;
//    EXPECT_TRUE(!b) << "Error: float is not an integral type";
//
//    b = lg::is_integral<unsigned>::value;
//    EXPECT_TRUE(b) << "Error: unsigned";
//
//    b = lg::is_integral<char>::value;
//    EXPECT_TRUE(!b) << "Error: char is not an integral type";
//}
//
//TEST(TypeTest, TypeIdIsCorrectForTypes)
//{
//    int i = 0;
//    lg::Type tInt = TYPE(int);
//    lg::Type ti = TYPEOF(i);
//    EXPECT_EQ(tInt, TYPE(int)) << "int is not int";
//    EXPECT_EQ(tInt, ti) << "TYPEOF int is not int";
//    
//    float f = 0.f;
//    lg::Type tFloat = TYPE(float);
//    lg::Type tf = TYPEOF(f);
//    EXPECT_EQ(tFloat, tf) << "TYPEOF float is not float";
//    EXPECT_NE(tFloat, tInt) << "an int should not be a float";
//    
//    double d = 0;
//    lg::Type tDouble = TYPE(double);
//    lg::Type td = TYPEOF(d);
//    EXPECT_EQ(tDouble, td);
//    EXPECT_NE(td, tInt);
//
//    char c = 'a';
//    lg::Type tChar = TYPE(char);
//    lg::Type tc = TYPEOF(c);
//    EXPECT_EQ(tChar, tc);
//    EXPECT_NE(tChar, TYPE(bool));
//
//    int* ip = NULL;
//    lg::Type tIntp = TYPE(int*);
//    lg::Type tip = TYPEOF(ip);
//    EXPECT_EQ(tInt, tip) << "pointer type failed to remove pointer";
//    EXPECT_NE(tIntp, tip) << "pointer type failed to remove pointer";
//
//    const unsigned cuv = 37;
//    lg::Type tcUnsigned = TYPE(const unsigned);
//    lg::Type tCuv = TYPEOF(cuv);
//    EXPECT_EQ(tCuv, TYPE(unsigned)) << "const failed";
//    EXPECT_NE(tcUnsigned, tCuv) << "const failed";
//
//    volatile bool vb = false;
//    lg::Type tvBool = TYPE(volatile bool);
//    lg::Type tvVb = TYPEOF(vb);
//    EXPECT_EQ(tvVb, TYPE(bool)) << "volatile failed";
//    EXPECT_NE(tvVb, tvBool) << "volatile failed";
//    
//    const volatile long* cvlp = NULL;
//    lg::Type cvLongPtr = TYPE(const volatile long*);
//    lg::Type tCvlp = TYPEOF(cvlp);
//    EXPECT_EQ(tCvlp, TYPE(long)) << "const volatile pointer failed to be removed";
//    EXPECT_NE(tCvlp, cvLongPtr) << "const volatile type failed to removed";
//}