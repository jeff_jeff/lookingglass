#pragma once


class someTestClass
{
public:
    int testInt;
};


/***********************************
This is one possible way to handle base class and 
derived class meta being correct from a pointer to
a base class that actually points to an instance of a
derived class.  The actual meta data itself here is
declared elsewhere just as you would any other class,
but getting meta from instances of these will access
the correct MetaInfoStorage template.  The class itself
though does NOT own the metaclass object
****************************************/
class someOtherTestClass
{
public:
    VIRTUAL_GETMETA(someOtherTestClass);
};

class someDerivedClass : public someOtherTestClass
{
public:
    VIRTUAL_GETMETA(someDerivedClass);
};

class baseClassVirtual
{
public:
    VIRTUAL_GETMETA(baseClassVirtual);
};


class derivedClassVirtual : public baseClassVirtual
{
public:
    VIRTUAL_GETMETA(derivedClassVirtual);
};
