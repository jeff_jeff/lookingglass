#include "stdafx.h"
#include "../include/IMetaClass.h"
#include "MetaTestDoubles.h"
#include "../include/MetaRegistry.h"


class MetaClassTest : public ::testing::Test
{
protected:
    void SetUp()
    {

    }

    void TearDown()
    {

    }
};


TEST_F(MetaClassTest, MetaDeclarationIsCorrectGlobalSingleton)
{
    lg::IMetaClass& mc = lg::MetaInfoStorage<someTestClass>::GetMeta();

    EXPECT_EQ(L"someTestClass", mc.Name());
    EXPECT_EQ(TYPE(someTestClass), mc.ClassType());
    EXPECT_EQ(sizeof(someTestClass), mc.Size());
}


TEST_F(MetaClassTest, DerivedClassMetaCorrectFromBaseClassInstance)
{
    someOtherTestClass sotc;
    lg::IMetaClass& mc2 = sotc.GetMeta();

    // this won't work due to compile time lookup
    // someOtherTestClass sotc = someDerivedClass();
    // sotc.GetMeta() -- will return SomeOtherTestClass meta
    // instead of someDerivedClassMeta.  No way around this though

    // this works fine though since it will do dynamic
    // lookup
    someDerivedClass sdc;
    someOtherTestClass& sotcd = sdc;

    lg::IMetaClass& mc3 = sotcd.GetMeta();

    EXPECT_NE(mc2.ClassType(), mc3.ClassType());

    EXPECT_EQ(TYPE(someOtherTestClass), mc2.ClassType());
    EXPECT_EQ(TYPE(someDerivedClass), mc3.ClassType());
}


TEST_F(MetaClassTest, ClassOwnedMeta_WithDerived_WorksCorrectly)
{
   
    baseClassVirtual* bcv_base = new baseClassVirtual();

    baseClassVirtual* bcv_derived = new derivedClassVirtual();

    lg::IMetaClass& mcbase = bcv_base->GetMeta();
    lg::IMetaClass& mcderived = bcv_derived->GetMeta();

    EXPECT_NE(mcbase.ClassType(), mcderived.ClassType());
    EXPECT_EQ(mcbase.ClassType(), TYPE(baseClassVirtual));
    EXPECT_EQ(mcbase.ClassType(), TYPEOF(bcv_base));
    EXPECT_EQ(mcderived.ClassType(), TYPE(derivedClassVirtual));
}

TEST(MetaLookup, GetMetaFromObject)
{
    // simple getting meta not owned by object
    someTestClass stc;
    lg::IMetaClass& metaFromStorage = OBJECT_META(stc);
    EXPECT_EQ(TYPE(someTestClass), metaFromStorage.ClassType());

    // late binding virtual GetMeta method with inheritance,
    // also owning its own metaclass object
    derivedClassVirtual dcv;
    lg::IMetaClass& metaFromVirtualDerived = OBJECT_META(dcv);

    EXPECT_EQ(TYPE(derivedClassVirtual), metaFromVirtualDerived.ClassType());

    baseClassVirtual bcv;
    lg::IMetaClass& metaFromVirtualBase = MetaLookup<decltype(bcv)>::resolve(bcv);

    EXPECT_EQ(TYPE(baseClassVirtual), metaFromVirtualBase.ClassType());
}

TEST(MetaLookup, GetMetaOnPlainType)
{
    lg::IMetaClass& scm = TYPE_META(baseClassVirtual);
    EXPECT_EQ(TYPE(baseClassVirtual), scm.ClassType());
}

TEST(MetaLookup, GetMetaOnObjectWithStaticGetTypeMeta)
{
    lg::IMetaClass& stcm = TYPE_META(someTestClass);
    EXPECT_EQ(TYPE(someTestClass), stcm.ClassType());
}