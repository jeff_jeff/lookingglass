========================================================================
    STATIC LIBRARY : LookingGlass Project Overview
========================================================================

LookingGlass is a c++ meta data and reflection library.  It provides the ability to get information about object's types, methods, properties, fields, etc. at runtime.  It requires c++ 11 functionality and there are no plans to back port this to earlier versions of the c++ standard.  It is originally written and tested on Windows 7 with Visual Studio 2010 but it is possible for it to work on other operating systems.

It is also the intent of this libary to be able to run with or without the STL, so the end user can use whatever containers they deem appropriate as well as whatever serialization and formatting they wish as well.  
